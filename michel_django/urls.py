from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('modulos_usuarios/', include('apps.modulos_usuarios.urls')),
    path('pacientes/', include('apps.pacientes.urls')),
    path('usuarios/', include('apps.usuarios.urls')),
    path('medicos/', include('apps.medicos.urls')),
    path('medicamento/', include('apps.medicamento.urls')),
    path('documento_antimicrobiano/', include('apps.documento_antimicrobiano.urls')),
    path('cultivo/', include('apps.cultivo.urls')),
    path('tratamiento/', include('apps.tratamiento.urls')),
    path('episodio/', include('apps.episodio.urls')),
    path('quimico_farmaceutico/', include('apps.quimico_farmaceutico.urls')),
]
