from django.db import models

from apps.perfiles.models import Perfiles

# Create your models here.
class Usuarios(models.Model):
    rutUsuario = models.CharField(max_length=50, primary_key=True)
    digitoVerificador = models.CharField(max_length=1,default=0)
    strNombreUsuario = models.CharField(max_length=255,default="Sin nombre")
    strApellidoPaternoUsuario = models.CharField(max_length=255,default="Sin apellido")
    strApellidoMaternoUsuario = models.CharField(max_length=255,default="Sin apellido")
    intPeso = models.IntegerField(null=True, default=0)
    strDireccionUsuario = models.CharField(max_length=255,default="Sin direccion")
    strSexo = models.CharField(max_length=255, null=True, default="Sin sexo")
    intEdad = models.IntegerField(null=True, default=0)
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    strContrasena = models.CharField(max_length=255)