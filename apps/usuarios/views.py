import json
from django.utils import timezone

from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.usuarios.models import Usuarios
from apps.pacientes.models import Pacientes
# Create your views here.

def getAll(request):

    usuarios = Usuarios.objects.all()
    
    usuarios_ordenados = []
    
    for usuario in usuarios:
        
        row = {}
        row["rutUsuario"] = usuario.rutUsuario
        row["strNombreUsuario"] = usuario.strNombreUsuario.capitalize()
        row["strApellidoUsuario"] = usuario.strApellidoUsuario.capitalize()
        row["strDireccionUsuario"] = usuario.strDireccionUsuario
        
        usuarios_ordenados.append(row)
        
    return JsonResponse(usuarios_ordenados, safe=False)

def getUsuariosNoPacientes(request):
    
    pacientes = Pacientes.objects.all()
    usuarios = Usuarios.objects.all().exclude(
        rutUsuario__in=Subquery(pacientes.values("usuario_id"))
    )
    
    usuarios_ordenados = []
    
    for usuario in usuarios:
        
        row = {}
        row["rutUsuario"] = usuario.rutUsuario
        row["strNombreUsuario"] = usuario.strNombreUsuario
        row["strApellidoUsuario"] = usuario.strApellidoPaternoUsuario + " " + usuario.strApellidoMaternoUsuario
        row["strDireccionUsuario"] = usuario.strDireccionUsuario
        
        usuarios_ordenados.append(row)
        
    return JsonResponse(usuarios_ordenados, safe=False)

def existeUsuario(request):
    
    POST = json.loads(request.body)
    
    # Recibir POST
    rutUsuario = POST["rutUsuario"]
    
    # Buscar rut en la tabla usuarios
    usuario = Usuarios.objects.filter(
        rutUsuario=rutUsuario
    )
    
    # Retornar true o false
    if usuario.count() == 0:
        return False
    elif usuario.count() > 0:
        return True
    
def agregarUsuario(request):
    
    POST = json.loads(request.body)
    
    rutUsuario = POST["rutUsuario"]
    digitoVerificador = POST["digitoVerificador"]
    strNombreUsuario = POST["strNombreUsuario"]
    strApellidoPaternoUsuario = POST["strApellidoPaternoUsuario"]
    strApellidoMaternoUsuario = POST["strApellidoMaternoUsuario"]
    intPeso = POST["intPeso"]
    strDireccionUsuario = POST["strDireccionUsuario"]
    strSexo = POST["strSexo"]
    intEdad = POST["intEdad"]
    dateFechaCreacion = timezone.now()
    timeHoraCreacion = timezone.now()
    dateFechaModificacion = timezone.now()
    timeHoraModificacion = timezone.now()
    
    usuario = Usuarios(
        rutUsuario=rutUsuario,
        digitoVerificador=digitoVerificador,
        strNombreUsuario=strNombreUsuario,
        strApellidoPaternoUsuario=strApellidoPaternoUsuario,
        strApellidoMaternoUsuario=strApellidoMaternoUsuario,
        intPeso=intPeso,
        strDireccionUsuario=strDireccionUsuario,
        strSexo=strSexo,
        intEdad=intEdad,
        dateFechaCreacion=dateFechaCreacion,
        timeHoraCreacion=timeHoraCreacion,
        dateFechaModificacion=dateFechaModificacion,
        timeHoraModificacion=timeHoraModificacion
    )
    
    try:
        usuario.save()
        print("usuario_agregado")
        return "usuario_agregado"
    except Exception as e:
        print("error_agregar_usuario")
        return "error_agregar_usuario"
    
def modificarUsuario(request):
    
    POST = json.loads(request.body)
    
    rutUsuarioOriginal = POST["rutUsuarioOriginal"]
    rutUsuario = POST["rutUsuario"]
    digitoVerificador = POST["digitoVerificador"]
    strNombreUsuario = POST["strNombreUsuario"]
    strApellidoPaternoUsuario = POST["strApellidoPaternoUsuario"]
    strApellidoMaternoUsuario = POST["strApellidoMaternoUsuario"]
    intPeso = POST["intPeso"]
    strDireccionUsuario = POST["strDireccionUsuario"]
    strSexo = POST["strSexo"]
    intEdad = POST["intEdad"]
    dateFechaModificacion = timezone.now()
    timeHoraModificacion = timezone.now()
    
    usuario = Usuarios.objects.get(
        rutUsuario=rutUsuarioOriginal
    )

    # usuario.rutUsuario = rutUsuario
    usuario.digitoVerificador = digitoVerificador
    usuario.strNombreUsuario = strNombreUsuario
    usuario.strApellidoPaternoUsuario = strApellidoPaternoUsuario
    usuario.strApellidoMaternoUsuario = strApellidoMaternoUsuario
    usuario.intPeso = intPeso
    usuario.strDireccionUsuario = strDireccionUsuario
    usuario.strSexo = strSexo
    usuario.intEdad = intEdad
    usuario.dateFechaModificacion = dateFechaModificacion
    usuario.timeHoraModificacion = timeHoraModificacion
    
    try:
        usuario.save(update_fields=[
            "digitoVerificador",
            "strNombreUsuario",
            "strApellidoPaternoUsuario",
            "strApellidoMaternoUsuario",
            "intPeso",
            "strDireccionUsuario",
            "strSexo",
            "intEdad",
            "dateFechaModificacion",
            "timeHoraModificacion",
        ])
        return "usuario_modificado"
    except Exception as e:
        print(e)
        return "error_modificar_usuario"