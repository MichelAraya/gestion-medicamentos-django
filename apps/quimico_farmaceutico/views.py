from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.quimico_farmaceutico.models import QuimicoFarmaceutico

# Create your views here.
def getAll(request):

    quimico_farmaceuticos = QuimicoFarmaceutico.objects.all()
    
    quimico_farmaceutico_ordenados = []
    
    for quimico_farmaceutico in quimico_farmaceuticos:
        
        row = {}
        row["rutUsuario"] = quimico_farmaceutico.usuario.rutUsuario
        row["strNombreUsuario"] = quimico_farmaceutico.usuario.strNombreUsuario.capitalize()
        row["strApellidoUsuario"] = quimico_farmaceutico.usuario.strApellidoPaternoUsuario.capitalize() + " " + quimico_farmaceutico.usuario.strApellidoMaternoUsuario.capitalize()
        row["strDireccionUsuario"] = quimico_farmaceutico.usuario.strDireccionUsuario
        row["strCargo"] = quimico_farmaceutico.strCargo
        
        quimico_farmaceutico_ordenados.append(row)
        
    return JsonResponse(quimico_farmaceutico_ordenados, safe=False)

