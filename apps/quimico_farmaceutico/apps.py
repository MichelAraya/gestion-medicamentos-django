from django.apps import AppConfig


class QuimicoFarmaceuticoConfig(AppConfig):
    name = 'quimico_farmaceutico'
