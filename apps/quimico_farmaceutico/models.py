from django.db import models

from apps.usuarios.models import Usuarios

# Create your models here.
class QuimicoFarmaceutico(models.Model):
    id = models.AutoField(primary_key=True)
    usuario = models.OneToOneField(Usuarios, on_delete=models.CASCADE, db_column="usuario_rutUsuario")
    strCargo = models.CharField(max_length=255)
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "quimico_farmaceutico"