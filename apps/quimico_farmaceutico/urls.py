from django.urls import path
from django.contrib import admin
from apps.quimico_farmaceutico.views import getAll

urlpatterns = [
    path('all', getAll),
]
