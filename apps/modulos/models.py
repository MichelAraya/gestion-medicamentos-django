from django.db import models

# Create your models here.
class Modulos(models.Model):
    strCodigoModulo = models.CharField(max_length=255, primary_key=True)
    strTextoModulo = models.CharField(max_length=255)
    strModulo = models.CharField(max_length=255)
    strIconoModulo = models.CharField(max_length=255)
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
