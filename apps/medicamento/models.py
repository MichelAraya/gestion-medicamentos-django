from django.db import models


# Create your models here.
class Medicamento(models.Model):
    id = models.AutoField(primary_key=True)
    strNombre = models.CharField(max_length=255)
    strDosis = models.CharField(max_length=255)
    intFrecuencia = models.IntegerField()
    intNumDias = models.IntegerField()
    boolAM = models.BooleanField()
    boolFA = models.BooleanField()
    strDescripcion = models.CharField(max_length=255)
        
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "medicamento"