from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.medicamento.models import Medicamento
import json


# Create your views here.
def getAll(request):

    medicamentos = Medicamento.objects.all()
    
    medicamentos_ordenados = []
    
    for medicamento in medicamentos:
        
        row = {}
        row["id"] = medicamento.id
        row["strNombre"] = medicamento.strNombre
        row["strDosis"] = medicamento.strDosis
        row["intFrecuencia"] = medicamento.intFrecuencia
        row["intNumDias"] = medicamento.intNumDias
        row["boolAM"] = "SI" if medicamento.boolAM else "NO"
        row["boolFA"] = "SI" if medicamento.boolFA else "NO"
        row["strDescripcion"] = medicamento.strDescripcion
        
        medicamentos_ordenados.append(row)
        
    return JsonResponse(medicamentos_ordenados, safe=False)

def getUsuariosNoPacientes(request):
    
    pacientes = Pacientes.objects.all()
    usuarios = Usuarios.objects.all().exclude(
        rutUsuario__in=Subquery(pacientes.values("usuario_id"))
    )
    
    usuarios_ordenados = []
    
    for usuario in usuarios:
        
        row = {}
        row["rutUsuario"] = usuario.rutUsuario
        row["strNombreUsuario"] = usuario.strNombreUsuario
        row["strApellidoUsuario"] = usuario.strApellidoUsuario
        row["strDireccionUsuario"] = usuario.strDireccionUsuario
        
        usuarios_ordenados.append(row)
        
    return JsonResponse(usuarios_ordenados, safe=False)