from django.urls import path
from django.contrib import admin
from apps.medicamento.views import getAll
from apps.usuarios.views import getUsuariosNoPacientes

urlpatterns = [
    path('all', getAll),
    path('no/pacientes', getUsuariosNoPacientes),
]
