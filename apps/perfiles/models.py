from django.db import models

# Create your models here.
class Perfiles(models.Model):
    intIdPerfil = models.AutoField(primary_key=True)
    strPerfil = models.CharField(max_length=255)