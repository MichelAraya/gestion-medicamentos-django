from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.documento_antimicrobiano.models import DocumentoAntimicrobiano
import json


# Create your views here.
def getAll(request):

    docAMs = DocumentoAntimicrobiano.objects.all()
    
    docAM_ordenados = []
    
    for docAM in docAMs:
        
        row = {}
        row["id"] = docAM.id
        row["strEstadoDoc"] = docAM.strEstadoDoc
        row["strAlergia"] = docAM.strAlergia
        row["strDiagnosticoInfeccion"] = docAM.strDiagnosticoInfeccion
        row["strHospitalizacion"] = docAM.strHospitalizacion
        row["boolCVC"] = "SI" if docAM.boolCVC == True else "NO" 
        row["boolVM"] = "SI" if docAM.boolVM == True else "NO"
        row["boolCup"] = "SI" if docAM.boolCup == True else "NO"
        row["strAgenteAislado"] = docAM.strAgenteAislado
        
        docAM_ordenados.append(row)
        
    return JsonResponse(docAM_ordenados, safe=False)