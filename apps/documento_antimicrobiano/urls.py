from django.urls import path
from django.contrib import admin
from apps.documento_antimicrobiano.views import getAll

urlpatterns = [
    path('all', getAll),
]
