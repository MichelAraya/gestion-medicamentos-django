from django.db import models

# Create your models here.
class DocumentoAntimicrobiano(models.Model):
    id = models.AutoField(primary_key=True)
    strEstadoDoc = models.CharField(max_length=255)
    strAlergia = models.CharField(max_length=255)
    strDiagnosticoInfeccion = models.CharField(max_length=255)
    strHospitalizacion = models.CharField(max_length=255)
    boolCVC = models.BooleanField(null=False)
    boolVM = models.BooleanField(null=False)
    boolCup = models.BooleanField(null=False)
    strAgenteAislado = models.CharField(max_length=255, null=False)
        
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "documento_antimicrobiano"