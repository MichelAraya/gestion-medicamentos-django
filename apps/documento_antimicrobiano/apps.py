from django.apps import AppConfig


class DocumentoAntimicrobianoConfig(AppConfig):
    name = 'documento_antimicrobiano'
