from django.db import models

from apps.documento_antimicrobiano.models import DocumentoAntimicrobiano

# Create your models here.
class SolicitudAntimicrobiano(models.Model):
    id = models.AutoField(primary_key=True)
    documento_antimicrobiano = models.OneToOneField(DocumentoAntimicrobiano, on_delete=models.CASCADE, db_column="documento_id")

    strDescripcion = models.CharField(max_length=255, null=False)
        
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "solicitud_antimicrobiano"