from django.apps import AppConfig


class SolicitudAntimicrobianoConfig(AppConfig):
    name = 'solicitud_antimicrobiano'
