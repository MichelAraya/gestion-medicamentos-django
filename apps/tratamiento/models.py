from django.db import models

from apps.usuarios.models import Usuarios
from apps.solicitud_antimicrobiano.models import SolicitudAntimicrobiano
from apps.cultivo.models import Cultivo

# Create your models here.
class Tratamiento(models.Model):
    id = models.AutoField(primary_key=True)
    medicos = models.OneToOneField(Usuarios, on_delete=models.CASCADE, db_column="usuarios_rutUsuario")
    solicitud = models.OneToOneField(SolicitudAntimicrobiano, on_delete=models.CASCADE, db_column="solicitud_id")
    cultivo = models.OneToOneField(Cultivo, on_delete=models.CASCADE, db_column="cultivo_id")
    boolExtraHosp = models.BooleanField(null=False)
    boolIntraHosp = models.BooleanField(null=False) 
    strDescripcion = models.CharField(null=False, max_length=255)
    strJustificacion = models.CharField(null=False, max_length=255)
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "tratamiento"