from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.tratamiento.models import Tratamiento
from apps.usuarios.models import Usuarios
from apps.medicos.models import Medicos
# Create your views here.
def getAll(request):

    tratamientos = Tratamiento.objects.all()
    
    tratamientos_ordenados = []
    
    for tratamiento in tratamientos:
        
        row = {}
        row["id"] = tratamiento.id
        row["boolExtraHosp"] = "SI" if tratamiento.boolExtraHosp else "NO"
        row["boolIntraHosp"] = "SI" if tratamiento.boolIntraHosp else "NO"
        row["strDescripcion"] = tratamiento.strDescripcion
        row["strJustificacion"] = tratamiento.strJustificacion
        row["dateFechaCreacion"] = tratamiento.dateFechaCreacion
        row["cultivo_id"] = tratamiento.cultivo_id
        row["solicitud_id"] = tratamiento.solicitud_id
        
        tratamientos_ordenados.append(row)
        
    return JsonResponse(tratamientos_ordenados, safe=False)
