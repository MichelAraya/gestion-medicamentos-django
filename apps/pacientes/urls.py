from django.urls import path
from django.contrib import admin
from apps.pacientes.views import getAll, agregar, modificarPaciente, eliminarPaciente

urlpatterns = [
    path('all', getAll),
    path('agregar', agregar),
    path('modificar', modificarPaciente),
    path('eliminar', eliminarPaciente),
]
