from django.db import models

from apps.usuarios.models import Usuarios
from apps.medicos.models import Medicos

# Create your models here.
class Pacientes(models.Model):
    usuario = models.OneToOneField(Usuarios, primary_key=True, on_delete=models.CASCADE, db_column="usuarios_rutUsuario")
    medicos = models.ForeignKey(Medicos, on_delete=models.CASCADE, db_column="medicos_rutUsuario", null=True)
    strProblema = models.CharField(max_length=255, null=True)
    # strMedicamento = models.CharField(max_length=255, null=True)
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "pacientes"