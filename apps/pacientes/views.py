from django.http import HttpResponse, JsonResponse
from apps.pacientes.models import Pacientes
from apps.medicos.models import Medicos
from apps.usuarios.models import Usuarios
from apps.usuarios.views import agregarUsuario, existeUsuario
from django.utils import timezone
import json

# Create your views here.
def getAll(request):
    
    pacientes = Pacientes.objects.all()
    
    pacientes_ordenados = []
    
    for paciente in pacientes:
        
        row = {}
        row["rutUsuario"] = paciente.usuario.rutUsuario
        row["strNombreUsuario"] = paciente.usuario.strNombreUsuario
        row["strApellidoUsuario"] = paciente.usuario.strApellidoPaternoUsuario + " " + paciente.usuario.strApellidoMaternoUsuario
        row["strDireccionUsuario"] = paciente.usuario.strDireccionUsuario
        row["strSexoUsuario"] = paciente.usuario.strSexo
        row["intEdadUsuario"] = paciente.usuario.intEdad
        row["strProblema"] = paciente.strProblema
        row["strDoctorDesignado"] = paciente.medicos.usuario.strNombreUsuario + " " +  paciente.medicos.usuario.strApellidoPaternoUsuario
        
        pacientes_ordenados.append(row)
        
    return JsonResponse(pacientes_ordenados, safe=False)
   
def agregar(request):
    
    # Recibir POST
    POST = json.loads(request.body)
    
    # Respuesta
    strRespuesta = ""

    # Consultar si existe el usuario en base de datos
    boolExisteUsuario = existeUsuario(request)
    
    # Si no existe, agregar a tabla usuarios
    if not boolExisteUsuario:
        respuesta = agregarUsuario(request)

        if respuesta == "usuario_agregado":
            strRespuesta = agregarPaciente(request)
            
        else:
            strRespuesta = respuesta
            
    elif boolExisteUsuario:
        strRespuesta = agregarPaciente(request)
    
    return JsonResponse(strRespuesta, safe=False)
    
# Agregar paciente
def agregarPaciente(request):

    POST = json.loads(request.body)
    
    paciente = Pacientes(
        usuario=Usuarios(rutUsuario=POST["rutUsuario"]),
        strProblema=POST["strProblema"],
        dateFechaCreacion=timezone.now(),
        timeHoraCreacion=timezone.now(),
        dateFechaModificacion=timezone.now(),
        timeHoraModificacion=timezone.now(),
        medicos=Medicos(id=POST["rutDoctor"])
    )
    
    try:
        paciente.save()
        respuesta = "paciente_creado"
    except Exception as e:
        respuesta = "error_agregar_paciente"
        
    return respuesta

# Modificar paciente
def modificarPaciente(request):
    
    POST = json.loads(request.body)
    
    try:
        
        Usuarios.objects.filter(
            rutUsuario=POST["rutPacienteOriginal"]
        ).update(
            strNombreUsuario=POST["strNombre"],
            strApellidoUsuario=POST["strApellido"],
            strDireccionUsuario=POST["strDireccion"],
            strSexo=POST["strSexo"],
            intEdad=POST["intEdad"],
            dateFechaModificacion=timezone.now(),
            timeHoraModificacion=timezone.now()
        )
        
        paciente = Pacientes.objects.filter(
            usuario_id=POST["rutPacienteOriginal"]
        ).update(
            usuario_id=POST["rutPaciente"],
            strProblema=POST["strProblema"],
            strMedicamento=POST["strMedicamento"],
            dateFechaModificacion=timezone.now(),
            timeHoraModificacion=timezone.now()
        )
        
        respuesta = "paciente_modificado"
    except Exception as e:
        respuesta = "error_modificar_paciente"
        
    row = {}
    row["jsonResponse"] = respuesta
    
    return JsonResponse(row, safe=False)

# Eliminar paciente
def eliminarPaciente(request):
    
    POST = json.loads(request.body)
    
    try:
        paciente = Pacientes.objects.filter(
            usuario_id=POST["rutPaciente"]
        ).delete()
        
        respuesta = "paciente_eliminado"
    except Exception as e:
        print(e)
        respuesta = "error_eliminar_paciente"
        
    row = {}
    row["jsonResponse"] = respuesta
    
    return JsonResponse(row, safe=False)

