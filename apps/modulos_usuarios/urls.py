from django.urls import path
from django.contrib import admin
from apps.modulos_usuarios.views import modulos_usuariosPorPerfil

urlpatterns = [
    path('por/perfil', modulos_usuariosPorPerfil)
]
