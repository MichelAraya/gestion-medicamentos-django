from django.apps import AppConfig


class ModulosUsuariosConfig(AppConfig):
    name = 'modulos_usuarios'
