# from django.shortcuts import render
# from django.core import serializers
from django.http import HttpResponse, JsonResponse
from apps.modulos_usuarios.dao_modulos_usuarios import dao_modulos_usuarios

# Create your views here.
def modulos_usuariosPorPerfil(request):
    
    # Mediante el DAO, accedemos a la base de datos para traer los módulos que corresponden
    # al perfil del usuario
    modulos_ordenados = dao_modulos_usuarios.modulos_usuariosPorPerfil()
    
    # Devolvemos una respuesta json
    return JsonResponse(modulos_ordenados, safe=False)

