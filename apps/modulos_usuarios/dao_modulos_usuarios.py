from apps.modulos_usuarios.models import ModulosUsuarios

class dao_modulos_usuarios():
    
    def modulos_usuariosPorPerfil():
        # Seleccionamos los módulos y sus relaciones
        modulos = ModulosUsuarios.objects.all().select_related("modulos","perfil")
        
        # Array que tendrá la data necesaria
        modulos_ordenados = []
            
        # Recorremos la información traida desde la bd
        for modulo in modulos:
            
            # Instanciamos un array vacío para agregarlo al array que se
            # devolverá en formato json
            row = {}
            row["strCodigoModulo"] = modulo.modulos.strCodigoModulo
            row["strTextoModulo"] = modulo.modulos.strTextoModulo
            row["strModulo"] = modulo.modulos.strModulo
            row["strPerfil"] = modulo.perfil.strPerfil
            row["strIconoModulo"] = modulo.modulos.strIconoModulo
            
            
            # agregamos la data
            modulos_ordenados.append(row)
            
        return modulos_ordenados