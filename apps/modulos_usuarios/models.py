from django.db import models

from apps.modulos.models import Modulos
from apps.perfiles.models import Perfiles
# Create your models here.
class ModulosUsuarios(models.Model):
    modulos = models.ForeignKey(Modulos, null=True, blank=True, on_delete=models.CASCADE)
    perfil = models.ForeignKey(Perfiles, null=True, blank=True, on_delete=models.CASCADE)