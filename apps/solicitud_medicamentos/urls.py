from django.urls import path
from django.contrib import admin
from apps.usuarios.views import getAll, getUsuariosNoPacientes

urlpatterns = [
    path('all', getAll),
    path('no/pacientes', getUsuariosNoPacientes),
]
