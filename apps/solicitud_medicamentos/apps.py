from django.apps import AppConfig


class SolicitudMedicamentosConfig(AppConfig):
    name = 'solicitud_medicamentos'
    