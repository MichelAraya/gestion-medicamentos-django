from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.usuarios.models import Usuarios
from apps.pacientes.models import Pacientes
# Create your views here.
def getAll(request):

    usuarios = Usuarios.objects.all()
    
    usuarios_ordenados = []
    
    for usuario in usuarios:
        
        row = {}
        row["rutUsuario"] = usuario.rutUsuario
        row["strNombreUsuario"] = usuario.strNombreUsuario.capitalize()
        row["strApellidoUsuario"] = usuario.strApellidoUsuario.capitalize()
        row["strDireccionUsuario"] = usuario.strDireccionUsuario
        
        usuarios_ordenados.append(row)
        
    return JsonResponse(usuarios_ordenados, safe=False)

def getUsuariosNoPacientes(request):
    
    pacientes = Pacientes.objects.all()
    usuarios = Usuarios.objects.all().exclude(
        rutUsuario__in=Subquery(pacientes.values("usuario_id"))
    )
    
    usuarios_ordenados = []
    
    for usuario in usuarios:
        
        row = {}
        row["rutUsuario"] = usuario.rutUsuario
        row["strNombreUsuario"] = usuario.strNombreUsuario
        row["strApellidoUsuario"] = usuario.strApellidoUsuario
        row["strDireccionUsuario"] = usuario.strDireccionUsuario
        
        usuarios_ordenados.append(row)
        
    return JsonResponse(usuarios_ordenados, safe=False)