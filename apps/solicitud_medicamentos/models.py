from django.db import models

from apps.usuarios.models import Usuarios
from apps.solicitud_antimicrobiano.models import SolicitudAntimicrobiano
from apps.medicamento.models import Medicamento

# Create your models here.
class SolicitudMedicamentos(models.Model):
    usuarios = models.ForeignKey(Usuarios, null=True, blank=True, on_delete=models.CASCADE)
    solicitudes = models.ForeignKey(SolicitudAntimicrobiano, null=True, blank=True, on_delete=models.CASCADE)
    Medicamentos = models.ForeignKey(Medicamento, null=True, blank=True, on_delete=models.CASCADE)
    
    class Meta:
        db_table = "solicitud_medicamentos"