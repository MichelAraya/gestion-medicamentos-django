from django.db import models

from apps.tratamiento.models import Tratamiento

# Create your models here.
class Episodio(models.Model):
    id = models.AutoField(primary_key=True)
    tratamiento = models.OneToOneField(Tratamiento, on_delete=models.CASCADE, db_column="tratamiento_id")
    dateFechaInicio = models.DateField()
    strServicio = models.CharField(max_length=255)
    strCama = models.CharField(max_length=255)
    strDireccion = models.CharField(max_length=255)
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "episodio"