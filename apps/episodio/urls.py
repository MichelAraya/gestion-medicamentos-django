from django.urls import path
from django.contrib import admin
from apps.episodio.views import getAll

urlpatterns = [
    path('all', getAll),
]
