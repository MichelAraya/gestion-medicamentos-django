from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.episodio.models import Episodio

# Create your views here.
def getAll(request):

    episodios = Episodio.objects.all()
    
    episodios_ordenados = []
    
    for episodio in episodios:
        
        row = {}
        row["id"] = episodio.id
        row["dateFechaInicio"] = episodio.dateFechaInicio
        row["strServicio"] = episodio.strServicio
        row["strCama"] = episodio.strCama
        row["strDireccion"] = episodio.strDireccion
        
        episodios_ordenados.append(row)
        
    return JsonResponse(episodios_ordenados, safe=False)
