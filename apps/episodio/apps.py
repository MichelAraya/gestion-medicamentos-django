from django.apps import AppConfig


class EpisodioConfig(AppConfig):
    name = 'episodio'
