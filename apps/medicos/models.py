from django.db import models

from apps.usuarios.models import Usuarios

# Create your models here.
class Medicos(models.Model):
    usuario = models.OneToOneField(Usuarios, on_delete=models.CASCADE, db_column="usuarios_rutUsuario")
    strCargo = models.CharField(max_length=255)
    strEspecialidad = models.CharField(max_length=255)
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "medicos"