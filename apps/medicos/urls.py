from django.urls import path
from django.contrib import admin
from apps.medicos.views import getAll, agregar, modificar, eliminarMedico

urlpatterns = [
    path('all', getAll),
    path('agregar', agregar),
    path('modificar', modificar),
    path('eliminar', eliminarMedico),
]
