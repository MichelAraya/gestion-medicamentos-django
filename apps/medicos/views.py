import json
from django.utils import timezone

from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery

from apps.medicos.models import Medicos
from apps.usuarios.views import existeUsuario, agregarUsuario, modificarUsuario

from apps.usuarios.models import Usuarios
from django.db.utils import IntegrityError


# Create your views here.
def getAll(request):

    medicos = Medicos.objects.all()
    
    medicos_ordenados = []
    
    for medico in medicos:
        
        row = {}
        row["id"] = medico.id
        row["rutUsuario"] = medico.usuario.rutUsuario
        row["strNombreUsuario"] = medico.usuario.strNombreUsuario.capitalize()
        row["strApellidoUsuario"] = medico.usuario.strApellidoPaternoUsuario.capitalize() + " " + medico.usuario.strApellidoMaternoUsuario.capitalize()
        row["strCargo"] = medico.strCargo
        row["strEspecialidad"] = medico.strEspecialidad
        
        medicos_ordenados.append(row)
        
    return JsonResponse(medicos_ordenados, safe=False)

def agregar(request):
    
    # Recibir POST
    POST = json.loads(request.body)
    
    # Respuesta
    strRespuesta = ""

    # Consultar si existe el usuario en base de datos
    boolExisteUsuario = existeUsuario(request)
    
    # Si no existe, agregar a tabla usuarios
    if not boolExisteUsuario:
        respuesta = agregarUsuario(request)

        if respuesta == "usuario_agregado":
            strRespuesta = agregarMedico(request)
            
        else:
            strRespuesta = respuesta
            
    elif boolExisteUsuario:
        strRespuesta = agregarMedico(request)
    
    return JsonResponse(strRespuesta, safe=False)

def agregarMedico(request):
    
    # Recibir POST
    POST = json.loads(request.body)
    
    rutUsuario = POST["rutUsuario"]
    strCargo = POST["strCargo"]
    strEspecialidad = POST["strEspecialidad"]
    dateFechaCreacion = timezone.now()
    timeHoraCreacion = timezone.now()
    dateFechaModificacion = timezone.now()
    timeHoraModificacion = timezone.now()
    
    medico = Medicos(
        usuario=Usuarios(rutUsuario=rutUsuario),
        strCargo=strCargo,
        strEspecialidad=strEspecialidad,
        dateFechaCreacion=dateFechaCreacion,
        timeHoraCreacion=timeHoraCreacion,
        dateFechaModificacion=dateFechaModificacion,
        timeHoraModificacion=timeHoraModificacion
    )
    
    try:
        medico.save()
        return "medico_creado"
    except IntegrityError as e:
        return "medico_existe"
    except Exception:
        return "error_agregar_usuario"
    
def modificar(request):
    
    strRespuesta = modificarUsuario(request)
    
    if strRespuesta == "usuario_modificado":
        strRespuesta = modificarMedico(request)
        
    elif strRespuesta == "error_modificar_usuario":
        strRespuesta = strRespuesta
    
    return JsonResponse(strRespuesta, safe=False)   
    
def modificarMedico(request):
    
    # Recibir POST
    POST = json.loads(request.body)
    
    id = POST["id"]
    strCargo = POST["strCargo"]
    strEspecialidad = POST["strEspecialidad"]
    dateFechaModificacion = timezone.now()
    timeHoraModificacion = timezone.now()
    
    medico = Medicos(
        id=id
    )
    
    medico.strCargo = strCargo
    medico.strEspecialidad = strEspecialidad
    medico.dateFechaModificacion = dateFechaModificacion
    medico.timeHoraModificacion = timeHoraModificacion
    
    try:
        medico.save(update_fields=[
            "strCargo",
            "strEspecialidad",
            "dateFechaModificacion",
            "timeHoraModificacion"
        ])
        
        return "medico_modificado"

    except Exception as e:
        return "error_modificar_medico"
    
def eliminarMedico(request):
    
    # Recibir POST
    POST = json.loads(request.body)
    
    id = POST["id"]
    strRespuesta = "";
    
    try:
        Medicos.objects.filter(id=id).delete()
        strRespuesta = "medico_eliminado"
    except Exception as e:
        strRespuesta = "error_eliminar_medico"
        
    return JsonResponse(strRespuesta, safe=False)   