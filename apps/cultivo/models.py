from django.db import models

from apps.quimico_farmaceutico.models import QuimicoFarmaceutico

# Create your models here.
class Cultivo(models.Model):
    id = models.AutoField(primary_key=True)
    quimico_farmaceutico = models.OneToOneField(QuimicoFarmaceutico, on_delete=models.CASCADE, db_column="quimico_id")

    strNombre = models.CharField(max_length=255)
    dateFechaInicio = models.DateField()
    boolUrocultivo = models.BooleanField(null=False)
    boolHemocultivo = models.BooleanField(null=False)
    ccat = models.BooleanField(null=False)
    expectoracion = models.BooleanField(null=False)
    secrecion = models.BooleanField(null=False)
    descripcion = models.CharField(null=False, max_length=255)
        
    dateFechaCreacion = models.DateField()
    timeHoraCreacion = models.TimeField()
    dateFechaModificacion = models.DateField()
    timeHoraModificacion = models.TimeField()
    
    class Meta:
        db_table = "cultivo"