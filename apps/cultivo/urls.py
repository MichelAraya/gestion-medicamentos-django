from django.urls import path
from django.contrib import admin
from apps.cultivo.views import getAll

urlpatterns = [
    path('all', getAll),
]
