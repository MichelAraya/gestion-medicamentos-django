from django.http import HttpResponse, JsonResponse
from django.db.models import Subquery
from apps.cultivo.models import Cultivo
import json

# Create your views here.
def getAll(request):

    cultivos = Cultivo.objects.all()
    
    cultivos_ordenados = []
    
    for cultivo in cultivos:
        
        row = {}
        row["id"] = cultivo.id
        row["strNombre"] = cultivo.strNombre
        row["dateFechaInicio"] = cultivo.dateFechaInicio
        row["boolUrocultivo"] = "SI" if cultivo.boolUrocultivo == True else "NO"
        row["boolHemocultivo"] = "SI" if cultivo.boolHemocultivo == True else "NO"
        row["ccat"] = "SI" if cultivo.ccat else "NO"
        row["expectoracion"] = "SI" if cultivo.expectoracion else "NO"
        row["secrecion"] = "SI" if cultivo.secrecion else "NO"
        row["descripcion"] = cultivo.descripcion
        
        cultivos_ordenados.append(row)
        
    return JsonResponse(cultivos_ordenados, safe=False)